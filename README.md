# README #

### What is this repository for? ###

In this repo you will find 2 examples of auto tests:

1. To create confluence page


2. To set restrictions to this page 

### How to run and see results? ###

You can run the tests from terminal on Mac OS. But before you need to run several commands:
1. brew install ant


2. ant download-ivy


3. ant resolve


4. brew install chromedriver


5. ant run

--------
After tests execution you will be able to find **the report** in folder "test-output" -> Html