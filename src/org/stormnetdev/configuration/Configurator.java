package org.stormnetdev.configuration;

import org.stormnetdev.inputdata.ConfigurationParser;

public class Configurator {
	
	static ConfigurationParser configurationParser = new ConfigurationParser();
    
    public static String defaultBrowserType;
    public static String browserType;
    
    public static String screenShotsPath;
    
    public static String iOSVersion;
	public static String appPath;
	public static String appiumURL;
	public static String uDID;
	public static String bundleID;
	
	public static void setConfiguration() {
		browserType = configurationParser.getParameter("BrowserType");
		    
		screenShotsPath = "./test-output/html/screenshots/";
		/*TODO: To remove XSLX
		if (browserType.equalsIgnoreCase("APPIUM IOS") || browserType.equalsIgnoreCase("APPIUM IOS EMULATOR")) {
			iOSVersion = configurationParser.getParameter(configurationParser.readAppiumIOSConfigurationSheet(), "iOS Version");
			appPath = configurationParser.getParameter(configurationParser.readAppiumIOSConfigurationSheet(), "Application Path");
			appiumURL = configurationParser.getParameter(configurationParser.readAppiumIOSConfigurationSheet(), "Appium URL");
			uDID = configurationParser.getParameter(configurationParser.readAppiumIOSConfigurationSheet(), "UDID");
			bundleID = configurationParser.getParameter(configurationParser.readAppiumIOSConfigurationSheet(), "Bundle ID");
		}
		*/

	}
    
}
