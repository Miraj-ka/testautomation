package org.stormnetdev.inputdata;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.stormnetdev.reporter.Reporter;

public class ConfigurationParser {
	
	Properties prop = new Properties();
	InputStream input = null;
	
	public ConfigurationParser() {
		openConfigurationFile();
	}
	
	private void openConfigurationFile() {
		String configurationFilePath = "./test-input/config.properties";
		Reporter.logInfo("Using Configuration File: " + configurationFilePath);
		try {
			input = new FileInputStream(configurationFilePath);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			prop.load(input);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void readAppiumIOSConfigurationSheet() {
		//TODO: To implement IOS Config
	}
	
	public void readAppiumAndroidConfigurationSheet() {
		//TODO: To implement Android Config
	}
	
	public String getParameter(String parameterName) {
		Reporter.logStep("Setting parameter: " + parameterName);
		String parameterValue = prop.getProperty(parameterName);
		Reporter.logOperation("Value set: " + parameterValue);
		Reporter.logPassedOperation();
		return parameterValue;	
	}
	
}
