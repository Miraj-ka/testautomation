package org.stormnetdev.inputdata.testdata;

import org.jfairy.Fairy;


public class DataProvider {
	public static Fairy dataGenerator = Fairy.create();
	
	public static Fairy getGenerator() {
		return dataGenerator;
	}

}
