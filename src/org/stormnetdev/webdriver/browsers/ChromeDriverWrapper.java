package org.stormnetdev.webdriver.browsers;

import org.openqa.selenium.chrome.ChromeDriver;

//TODO: Class should implement browser interface with browser-specific methods e.g. renderXPath
public class ChromeDriverWrapper {
    
    public ChromeDriver getDriver() {
        return new ChromeDriver();
    }
    
}
