package org.stormnetdev.webdriver.browsers;

import org.openqa.selenium.firefox.FirefoxDriver;

public class FireFoxDriverWrapper {
    
    public FirefoxDriver getDriver() {
        return new FirefoxDriver();
    }
    
}
