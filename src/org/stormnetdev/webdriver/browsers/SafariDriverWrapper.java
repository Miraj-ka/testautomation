package org.stormnetdev.webdriver.browsers;

import org.openqa.selenium.safari.SafariDriver;

public class SafariDriverWrapper {
    
    public SafariDriver getDriver() {
        return new SafariDriver();
    }

}
