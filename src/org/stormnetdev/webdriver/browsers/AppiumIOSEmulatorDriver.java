package org.stormnetdev.webdriver.browsers;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.stormnetdev.configuration.Configurator;

public class AppiumIOSEmulatorDriver {
    
    public RemoteWebDriver getDriver() {
    	DesiredCapabilities capabilities = new DesiredCapabilities();
    	
    	//TODO: To refactor (move generic lines to AppiumGenericDriver)
    	//capabilities.setCapability(CapabilityType.BROWSER_NAME, "iOS");
    	capabilities.setCapability("deviceName", "iPhone Retina (4-inch 64-bit)");
    	capabilities.setCapability("platformName", "iOS");
    	capabilities.setCapability("platformVersion", Configurator.iOSVersion);
    	capabilities.setCapability("autoAcceptAlerts", true);
    	//capabilities.setCapability("locationServicesEnabled", true);
    	//capabilities.setCapability("locationServicesAuthorized", true);
    	capabilities.setCapability("bundleId", Configurator.bundleID);
    	capabilities.setCapability("app", Configurator.appPath);
       	//capabilities.setCapability("UDID", Configurator.uDID);
    	URL appiumURL = null;
		try {
			appiumURL = new URL(Configurator.appiumURL);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    		
		RemoteWebDriver driver = new RemoteWebDriver(appiumURL, capabilities);
		
        return driver;
    }
    
}
