package org.stormnetdev.webdriver.browsers;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.stormnetdev.configuration.Configurator;

public class AppiumIOSDriver {
    
    public RemoteWebDriver getDriver() {
    	DesiredCapabilities capabilities = new DesiredCapabilities();
    	
    	//TODO: To refactor (move generic lines to AppiumGenericDriver)
    	capabilities.setCapability(CapabilityType.BROWSER_NAME, "iOS");
    	capabilities.setCapability(CapabilityType.VERSION, Configurator.iOSVersion);
    	capabilities.setCapability("app", Configurator.appPath);
    	//capabilities.setCapability("udid", Configurator.uDID); //TODO: Not necessary? To investigate with terminal appium
    	//capabilities.setCapability("bundleid", Configurator.bundleID); //TODO: Not necessary? To investigate with terminal appium
    	
    	URL appiumURL = null;
		try {
			appiumURL = new URL(Configurator.appiumURL);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    		
		RemoteWebDriver driver = new RemoteWebDriver(appiumURL, capabilities);
		
        return driver;
    }
    
}
