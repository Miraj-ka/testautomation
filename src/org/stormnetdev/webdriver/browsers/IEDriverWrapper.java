package org.stormnetdev.webdriver.browsers;

import org.openqa.selenium.ie.InternetExplorerDriver;

public class IEDriverWrapper {
    
    public InternetExplorerDriver getDriver() {
        return new InternetExplorerDriver();
    }

}
