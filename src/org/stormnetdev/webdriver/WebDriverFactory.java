package org.stormnetdev.webdriver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.*;
import org.stormnetdev.configuration.Configurator;
import org.stormnetdev.reporter.Reporter;
import org.stormnetdev.webdriver.browsers.*;

public class WebDriverFactory {
    
    public static WebDriver driver;
    
    public static EventFiringWebDriver myDriver;
    
    public static WebDriverEventListener webDriverEventListener = new MyWebDriverListener();

    public static WebDriver getDriver() {
        return driver;
    }

    public static void setDriver(String browserType) {
    	Reporter.logOperation("Instantiating driver");
    	
    	//TODO: Make consistent with others (add wrapper or generic appium class)    	
        if (browserType.equalsIgnoreCase("APPIUM IOS")) {
        	AppiumIOSDriver driverWrapper = new AppiumIOSDriver();
        	WebDriverFactory.driver = driverWrapper.getDriver();
		}
        if (browserType.equalsIgnoreCase("APPIUM IOS EMULATOR")) {
        	AppiumIOSEmulatorDriver driverWrapper = new AppiumIOSEmulatorDriver();
        	WebDriverFactory.driver = driverWrapper.getDriver();
		}
        if (browserType.equalsIgnoreCase("CHROME")) {
            ChromeDriverWrapper driverWrapper = new ChromeDriverWrapper();
            WebDriverFactory.driver = driverWrapper.getDriver();
        }
        if (browserType.equalsIgnoreCase("FIREFOX")) {
            FireFoxDriverWrapper driverWrapper = new FireFoxDriverWrapper();
            driver = driverWrapper.getDriver();
        }
        if (browserType.equalsIgnoreCase("IE")) {
            IEDriverWrapper driverWrapper = new IEDriverWrapper();
            WebDriverFactory.driver = driverWrapper.getDriver();
        }
        
        if (browserType.equalsIgnoreCase("SAFARI")) {
            SafariDriverWrapper driverWrapper = new SafariDriverWrapper();
            WebDriverFactory.driver = driverWrapper.getDriver();
        }
        
        myDriver = new EventFiringWebDriver(getDriver()).register(webDriverEventListener);
        WebDriverFactory.driver = myDriver;
        
        if (driver != null) {
        	Reporter.logPassedOperation();
		} else {
			throw new NullPointerException("Driver has not been instantiated");
		}
        
    }
    
    public static void configureDriver() {
    	Reporter.logOperation("Configuring driver");
        getDriver().manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
        getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); 
        Reporter.logPassedOperation();
    }
    
    public static void instantiateBrowser() {
    	Reporter.logInfo("Webdriver: " + Configurator.browserType.toString());
        WebDriverFactory.setDriver(Configurator.browserType);        
    }
    
}
