package org.stormnetdev.webdriver;

import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.stormnetdev.configuration.Configurator;
import org.stormnetdev.reporter.Reporter;
import org.testng.Assert;
import org.testng.asserts.Assertion;
import org.testng.asserts.SoftAssert;

public abstract class WebDriverWrapper {
    
    public static Assertion hardAssert = new Assertion();
    public static SoftAssert softAssert = new SoftAssert();
    
    public static void openURL(String uRL) {
        WebDriverFactory.getDriver().get(uRL);
    }
    
    public static void refresh() {
    	WebDriverFactory.getDriver().navigate().refresh();
    }
    
    public static void restartBrowser() {
		closeBrowser();
		startBrowser();
	}
    
    public static void startBrowser() {
    	Configurator.setConfiguration();
        WebDriverFactory.instantiateBrowser();
        WebDriverFactory.configureDriver();
	}
    
    public static void cleanBrowser() {
    	WebDriverFactory.getDriver().manage().deleteAllCookies();
	}
    
    public static void closeBrowser() {
    	WebDriverFactory.getDriver().quit();
    }
    
    public static void switchToIframe(String frame) {
		WebDriverFactory.getDriver().switchTo().frame(frame);
	}
    
    public static void switchToDefaultFrame() {
		WebDriverFactory.getDriver().switchTo().defaultContent();
	}
    
    public static String getElementStringRepresentation(WebElement webElement) {
        //TODO: Change split to more compact RegEx
        return webElement.toString();        
    }
    
    public static void clickOnElement(WebElement webElement) {
        webElement.click();
    }
    
    public static void selectByVisibleText(WebElement webElement, String value) {
    	Select dropdown = new Select(webElement);
    	dropdown.selectByVisibleText(value);
    	
    }
    
    public static void fillTextField(WebElement fieldWebElement, String text) {
        Reporter.logOperation("Fill field with text: " + text);
        MyWebDriverListener.setNewValue(text);
        fieldWebElement.sendKeys(text);
    }
    
    public static WebElement findElement(By locator) throws NoSuchElementException {
        Reporter.logOperation("Find element: " + locator);
        
        try {
            WebElement element = WebDriverFactory.getDriver().findElement(locator);
            return element;  
        } catch (Exception e) {
           //Reporter.logFailed(e.getMessage());
           throw new RuntimeException(e);
        }
        
    }
    
    public static void typeText(String text) {
    	Reporter.logSubOperation("Typing text: " + text);
    	Actions builder = new Actions(WebDriverFactory.getDriver());
    	builder.sendKeys(text).build().perform();
    }
    
    public static void assertElementContainsText(By locator, String text) {
    	WebElement element = findElement(locator);
    	Reporter.logOperation("Highlighting element");
    	highlightElement(element);
    	Reporter.logOperation("Asserting element: " + locator + " contains text: " + text);
    	try {
    		getHardAssertion().assertTrue(element.getText().contains(text));
		} catch (AssertionError e) {
			Reporter.logFailed("Expected text: '" + text + "' is not found inside of element: " + locator);
			Assert.fail();
		}
	}
    
    public static void verifyElementContainsText(By locator, String text) {
    	//TODO: To fix and to add try-catch
    	WebElement element = findElement(locator);
    	Reporter.logOperation("Verifying element: " + locator + " contains text: " + text);
    	highlightElement(element);
		getSoftAssertion().assertTrue(element.getText().contains(text));
	}
    
    public static void highlightElement(WebElement element) { 
		for (int i = 0; i < 1; i++) { 
			JavascriptExecutor js = (JavascriptExecutor)WebDriverFactory.getDriver();
			 js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: 4px solid orange; border-style: dashed;"); 
		}
	}
    
    public static Assertion getHardAssertion(){
    	return hardAssert;
    }
    
    public static SoftAssert getSoftAssertion(){
    	return softAssert;
    }
    
    public static String getText(WebElement element) {
		return element.getText();
	}
    
    public static String getValue(WebElement element) {
		return element.getAttribute("Value");
	}
    
}
