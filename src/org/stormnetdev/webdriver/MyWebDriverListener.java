package org.stormnetdev.webdriver;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;
import org.stormnetdev.reporter.Reporter;

public class MyWebDriverListener implements WebDriverEventListener{

	private By lastFindBy;
	private String originalValue;
	private static String newValue;
	
	public static void setNewValue (String value) {
		MyWebDriverListener.newValue = value;
	}

	@Override
	public void afterChangeValueOf(WebElement element, WebDriver arg1) {
		Reporter.logSubOperation("Changed value to: '" + newValue + "'");
		Reporter.logPassedOperation();
		
	}

	@Override
	public void afterClickOn(WebElement arg0, WebDriver arg1) {
		Reporter.logPassedOperation();
		
	}

	@Override
	public void afterFindBy(By arg0, WebElement arg1, WebDriver arg2) {
		Reporter.logPassedOperation();
		
	}

	@Override
	public void afterNavigateBack(WebDriver arg0) {
		Reporter.logPassedOperation();
		
	}

	@Override
	public void afterNavigateForward(WebDriver arg0) {
		Reporter.logPassedOperation();
		
	}

	@Override
	public void afterNavigateTo(String arg0, WebDriver arg1) {
		Reporter.logPassedOperation();
	
	}

	@Override
	public void afterScript(String arg0, WebDriver arg1) {
		Reporter.logPassedOperation();
		
	}

	@Override
	public void beforeChangeValueOf(WebElement element, WebDriver arg1) {
		Reporter.logSubOperation("Changing value of: " +  lastFindBy);
		originalValue = element.getAttribute("value");
		if (originalValue.isEmpty()) {
			originalValue = "NULL";
		}
		Reporter.logSubOperation("Changing value from: '" + originalValue + "'");
		
	}

	@Override
	public void beforeClickOn(WebElement arg0, WebDriver arg1) {
		Reporter.logOperation("Click on element " +  lastFindBy);
		
	}

	@Override
	public void beforeFindBy(By by, WebElement arg1, WebDriver arg2) {
		lastFindBy = by;
		
	}

	@Override
	public void beforeNavigateBack(WebDriver arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeNavigateForward(WebDriver arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeNavigateTo(String uRL, WebDriver arg1) {
		Reporter.logOperation("Open '" + uRL + "' URL");
		
	}

	@Override
	public void beforeScript(String arg0, WebDriver arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onException(Throwable error, WebDriver arg1) {
		if (error.getClass().equals(NoSuchElementException.class)){
            Reporter.logFailed("WebDriver error: Element not found "+ lastFindBy);
        } else {
        	Reporter.logFailed("WebDriver error: ");
        }
	}

}
