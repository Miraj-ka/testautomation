package org.stormnetdev.reporter;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.stormnetdev.configuration.Configurator;
import org.stormnetdev.webdriver.WebDriverFactory;

public class Reporter {
    
	//TODO: To move all styles to report stylesheet
	
    public static void logInfo(String message) {
        System.out.println("Info: " + message);
        org.testng.Reporter.log("<h3 style='color: #FF6600;'><b>Info:</b> " + message + "</h3>");
    }
    
    public static void logOperation(String message) {
        System.out.print("    Operation: " + message);
        org.testng.Reporter.log("<p style='margin-left: 2em;'><b>Operation:</b> " + message + "</p>");
    }
    
    public static void logSubOperation(String message) {
        System.out.print("    > " + message);
        org.testng.Reporter.log("<p style='margin-left: 4em;'><b>></b> " + message + "</p>");
    }
    
    public static void logStep(String message) {
        System.out.println("Step: " + message);
        org.testng.Reporter.log("<h2 style='color: #006699;'><b>Step:</b> " + message + "</h2>");
    }
    
    public static void logPassedOperation() {
        System.out.println("    |    Passed");
        org.testng.Reporter.log("<p style='color: #339966; margin-left: 2em;'><b>> </b>Operation Passed</p>");
    }
    
    public static void logPassedStep() {
        System.out.println("Step Passed");
        org.testng.Reporter.log("<h2 style='color: #339966; padding-bottom: .5em;'><b>Step Passed</b></h2>");
    }
    
    public static void logFailed(String message) {
        System.out.println(message);
        org.testng.Reporter.log("<h3 style='color: #FF0033;'>" + message + "</h3>");
    }

	public static void logTest(String testName) {
		System.out.println("Test name: " + testName);
        org.testng.Reporter.log("<h2 style='color: #6A0888;'> Test name: " + testName + "</h2>");
	}
	
	public static void logTestPassed() {
		System.out.println("Test result: Passed");
        org.testng.Reporter.log("<h2 style='color: #339966;'>Test result: Passed</h2>");
        org.testng.Reporter.log("<hr />");
	}
	
	public static void logTestFailed() {
		System.out.println("Test result: Failed");
        org.testng.Reporter.log("<h2  style='color: #FF0033;'>Test result: Failed.</h2>");
        org.testng.Reporter.log("<hr />");
	}
	
	//TODO: To refactor
	public static void takeScreenShot(String testname) {
		try {
			File screenShotFile = null;
			
			if (Configurator.browserType.equalsIgnoreCase("APPIUM IOS") || Configurator.browserType.equalsIgnoreCase("APPIUM ANDROID") ||
					Configurator.browserType.equalsIgnoreCase("APPIUM IOS EMULATOR") || Configurator.browserType.equalsIgnoreCase("APPIUM ANDROID EMULATOR")) {
				WebDriver augmentedDriver = new Augmenter().augment(WebDriverFactory.getDriver());
				screenShotFile = ((TakesScreenshot)augmentedDriver).getScreenshotAs(OutputType.FILE);
			}else {
				screenShotFile = ((TakesScreenshot) WebDriverFactory.getDriver()).getScreenshotAs(OutputType.FILE);
			}
			
			String screenShotFileName = testname + ".png";
			String screenShotFilePath = Configurator.screenShotsPath + screenShotFileName;
			FileUtils.copyFile(screenShotFile, new File(screenShotFilePath));
            String screenShotFilePathReportNG = "./test-output/html/screenshots/";
            
            if (Configurator.screenShotsPath.equals(screenShotFilePathReportNG) != true) {
            	 FileUtils.copyFile(screenShotFile, new File(screenShotFilePathReportNG + screenShotFileName));
			}
            
            String screenShotFilePathHTML = "screenshots/" + screenShotFileName;
            
            org.testng.Reporter.log("<a href='" + screenShotFilePathHTML + "' target='_blank'><img height='400' alt='Click to open in new window' src='" +
            		screenShotFilePathHTML + "'></a>");
            
		} catch (IOException e) {
			logFailed("Cannot create screenshot");
			e.printStackTrace();
		}
		
	}
    
}
