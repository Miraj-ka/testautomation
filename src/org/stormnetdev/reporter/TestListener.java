package org.stormnetdev.reporter;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestListener implements ITestListener{

	@Override
	public void onFinish(ITestContext test) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStart(ITestContext test) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult test) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestFailure(ITestResult test) {
		Reporter.takeScreenShot(test.getName());
		Reporter.logTestFailed();		
	}

	@Override
	public void onTestSkipped(ITestResult test) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestStart(ITestResult test) {
		Reporter.logTest(test.getName());
	}

	@Override
	public void onTestSuccess(ITestResult test) {
		Reporter.takeScreenShot(test.getName());
		Reporter.logTestPassed();
	}

}
