package org.stormnetdev.pageobjects.sampletest;

import org.openqa.selenium.By;
import org.stormnetdev.inputdata.testdata.DataProvider;
import org.stormnetdev.pageobjects.CommonPageObject;
import org.stormnetdev.reporter.Reporter;

import static org.stormnetdev.webdriver.WebDriverWrapper.*;

public class GoogleHomePage extends CommonPageObject{

    public static boolean isPageOpened() {
        // TODO Auto-generated method stub
        return false;
    }
    
    public static void openPage() {
        Reporter.logStep("Open Google home page");
        openURL("http://google.com");
        Reporter.logPassedStep();
    }
    
    public static void performSearchQuery(String query) {
        Reporter.logStep("Perform search query: " + query);
        fillTextField(findElement(By.name("q")), query);
        Reporter.logPassedStep();
    }

	public static void submit() {
		Reporter.logStep("Pressing Submit button");
		clickOnElement(findElement(By.name("btnK")));
		Reporter.logPassedStep();
	}

	public static void verifySearchResultContains(String result) {
		Reporter.logStep("Asserting search results contains: " + result);
		assertElementContainsText(By.id("ires"), result);
		Reporter.logPassedStep();
	}

}
