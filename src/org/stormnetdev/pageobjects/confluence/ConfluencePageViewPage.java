package org.stormnetdev.pageobjects.confluence;

import org.openqa.selenium.By;
import org.stormnetdev.inputdata.testdata.DataProvider;
import org.stormnetdev.pageobjects.CommonPageObject;
import org.stormnetdev.reporter.Reporter;

import static org.stormnetdev.webdriver.WebDriverWrapper.*;

public class ConfluencePageViewPage extends CommonPageObject{

    public static boolean isPageOpened() {
        // TODO Auto-generated method stub
        return false;
    }

	public static void verifyPageCreated(String title, String content) {
		Reporter.logStep("Verifying page created");
		verifyElementContainsText(By.id("title-text"), title);
		verifyElementContainsText(By.id("main-content"), content);
		Reporter.logPassedStep();
	}

	public static void setRestrictions(String restrictions) {
		Reporter.logStep("Setting page restrictions to: " + restrictions);
		clickOnElement(findElement(By.id("content-metadata-page-restrictions")));
		selectByVisibleText(findElement(By.id("page-restrictions-dialog-selector")), restrictions);
		clickOnElement(findElement(By.id("page-restrictions-dialog-save-button")));
		Reporter.logPassedStep();
	}

	public static void verifyRestrictionsSet(String restrictionsDescription) {
		Reporter.logStep("Verifying page restrictiona are: " + restrictionsDescription);
		refresh();
		clickOnElement(findElement(By.id("content-metadata-page-restrictions")));
		verifyElementContainsText(By.className("page-restrictions-dialog-explanation"), restrictionsDescription);
		Reporter.logPassedStep();
	}

}
