package org.stormnetdev.pageobjects.confluence;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.server.handler.SendKeys;
import org.stormnetdev.inputdata.testdata.DataProvider;
import org.stormnetdev.pageobjects.CommonPageObject;
import org.stormnetdev.reporter.Reporter;

import static org.stormnetdev.webdriver.WebDriverWrapper.*;

import java.time.Instant;

public class ConfluenceHomePage extends CommonPageObject{

    public static boolean isPageOpened() {
        // TODO Auto-generated method stub
        return false;
    }

	public static void openPage(String url) {
		Reporter.logStep("Open Confluence home page");
        openURL(url);
        Reporter.logPassedStep();
	}

	public static void createPage(String title, String content) {
		Instant instant = Instant.now (); // Current date-time in UTC.
		String date = instant.toString ();
		Reporter.logStep("Creating page with title: " + title + " " + date + " and content: " + content);
		clickOnElement(findElement(By.id("quick-create-page-button")));
		fillTextField(findElement(By.id("content-title")), title + " " + date);
		clickOnElement(findElement(By.id("wysiwygTextarea_ifr")));
		typeText(content);
		clickOnElement(findElement(By.id("rte-button-publish")));
		Reporter.logPassedStep();
	}

}
