package org.stormnetdev.pageobjects.confluence;

import org.openqa.selenium.By;
import org.stormnetdev.inputdata.testdata.DataProvider;
import org.stormnetdev.pageobjects.CommonPageObject;
import org.stormnetdev.reporter.Reporter;

import static org.stormnetdev.webdriver.WebDriverWrapper.*;

public class ConfluenceLoginPage extends CommonPageObject{

    public static boolean isPageOpened() {
        // TODO Auto-generated method stub
        return false;
    }

	public static void authorize(String login, String password) {
		Reporter.logStep("Authorizing with login: " + login + " and password: " + password);
		fillTextField(findElement(By.name("username")), login);
		fillTextField(findElement(By.name("password")), password);
		clickOnElement(findElement(By.id("login")));
        Reporter.logPassedStep();
	}

}
