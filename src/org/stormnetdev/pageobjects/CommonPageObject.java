package org.stormnetdev.pageobjects;

import org.stormnetdev.reporter.Reporter;
import static org.stormnetdev.webdriver.WebDriverWrapper.*;

public class CommonPageObject {
    
    //Check if corresponding page is opened. To be overriden in subclass
    private static boolean isPageOpened() {
        return false;
    }
    
}
