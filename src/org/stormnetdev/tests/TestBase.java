package org.stormnetdev.tests;

import static org.stormnetdev.webdriver.WebDriverWrapper.*;

import org.testng.annotations.*;

public abstract class TestBase {
	
	@BeforeSuite
    public void setUpSuite() {
    	startBrowser();
    }
    
    @BeforeTest
    public void setUpTest() {
    }
    
    @AfterTest
    public void tearDownTest() throws InterruptedException {
    	cleanBrowser();
    }
    
    @AfterSuite
    public void tearDownSuite() {
        closeBrowser();
        //getSoftAssertion().assertAll();
    }
    
}
