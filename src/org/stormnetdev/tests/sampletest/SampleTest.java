package org.stormnetdev.tests.sampletest;

import org.stormnetdev.tests.TestBase;
import org.stormnetdev.pageobjects.sampletest.*;
import org.testng.annotations.Test;

public class SampleTest extends TestBase {

    @Test
    public void samplePassedTest() {
        GoogleHomePage.openPage();
        GoogleHomePage.performSearchQuery("Test automation");
        GoogleHomePage.submit();
        GoogleHomePage.verifySearchResultContains("Test automation - Wikipedia");
    }
    
    @Test
    public void sampleFailedTest() {
        GoogleHomePage.openPage();
        GoogleHomePage.performSearchQuery("Test automation");
        GoogleHomePage.submit();
        GoogleHomePage.verifySearchResultContains("Test automation - Aidepikiw");
    }

}