package org.stormnetdev.tests.confluence;

import org.stormnetdev.tests.TestBase;
import org.stormnetdev.pageobjects.confluence.*;
import org.testng.annotations.Test;

public class ConfluenceTestPageCreation extends TestBase {
	@Test
	public void createPageTest() {
		ConfluenceHomePage.openPage("https://melon16.atlassian.net/wiki/");
		ConfluenceLoginPage.authorize("natalie.shakra@gmail.com", "Summer12");
		ConfluenceHomePage.createPage("Hello, fellow humans.", "Bots will prevail!");
		ConfluencePageViewPage.verifyPageCreated("Hello, fellow humans.", "Bots will prevail!");
	}
}
