package org.stormnetdev.tests.confluence;

import org.stormnetdev.tests.TestBase;
import org.stormnetdev.pageobjects.confluence.*;
import org.testng.annotations.Test;

public class ConfluenceTestPageRestrictionsSetting extends TestBase {
	@Test
	public void setRestrictionsTest() {
		ConfluenceHomePage.openPage("https://melon16.atlassian.net/wiki/");
		ConfluenceLoginPage.authorize("natalie.shakra@gmail.com", "Summer12");
		ConfluenceHomePage.createPage("Page with restrictions.", "This page should be restricted.");
		ConfluencePageViewPage.setRestrictions("Viewing and editing restricted");
		ConfluencePageViewPage.verifyRestrictionsSet("Only some people can view or edit.");
	}
}
